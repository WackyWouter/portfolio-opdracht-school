height = $(window).height();
console.log(height);
current = "#home";
toggle1 = 1;
toggle2 = 1;
toggle3 = 1;


$("#grid-1").css("grid-template-rows", height);

$(window).on('resize', function(){
  height = $(window).height();
  ($("#grid-1").css("grid-template-rows", height) );
});

function emailValidation(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}



$("#changeOp1").click(function(){
  if (toggle1 == 1 ){
    $("#opdracht1").addClass("opdracht1B");
    $("#imgOp1").css("grid-area", "");
    $("#imgOp1").css("display", "none");
    $("#textOp1").css("grid-area", "textOp1");
    $("#textOp1").css("display", "block");
    toggle1++
    $("#changeOp1").html("Design");
  }else {
    $("#opdracht1").removeClass("opdracht1B");
    toggle1--;
    $("#imgOp1").css("grid-area", "imgOp1");
    $("#imgOp1").css("display", "block");
    $("#textOp1").css("grid-area", "");
    $("#textOp1").css("display", "none");
    $("#changeOp1").html("Extra info");
  }
});

$("#changeOp2").click(function(){
  if (toggle2 == 1 ){
    $("#opdracht2").addClass("opdracht2B");
    $("#imgOp2").css("grid-area", "");
    $("#imgOp2").css("display", "none");
    $("#textOp2").css("grid-area", "textOp2");
    $("#textOp2").css("display", "block");
    toggle2++
    $("#changeOp2").html("Design");
  }else {
    $("#opdracht2").removeClass("opdracht2B");
    $("#imgOp2").css("grid-area", "imgOp2");
    $("#imgOp2").css("display", "block");
    $("#textOp2").css("grid-area", "");
    $("#textOp2").css("display", "none");
    toggle2--;
    $("#changeOp1").html("Extra info");
  }
});

$("#changeOp3").click(function(){
  if (toggle3 == 1 ){
    $("#opdracht3").addClass("opdracht3B");
    $("#imgOp3A").css("grid-area", "");
    $("#imgOp3B").css("grid-area", "");
    $(".imgOp3").css("display", "none");
    $("#textOp3").css("grid-area", "textOp3");
    $("#textOp3").css("display", "block");;
    toggle3++
    $("#changeOp3").html("Design");
  }else {
    $("#opdracht3").removeClass("opdracht3B");
    $("#imgOp3A").css("grid-area", "imgOp3A");
    $("#imgOp3B").css("grid-area", "imgOp3B");
    $(".imgOp3").css("display", "block");
    $("#textOp3").css("grid-area", "");
    $("#textOp3").css("display", "none");
    toggle3--;
    $("#changeOp3").html("Extra info");
  }
});

$("#submit").click(function(){
  var email = $("#emailInput").val();
  var antwoord = ""
  var emailCheck = true;
  if (($("#nameInput").val() == "") || ($("#emailInput").val() == "") || ($("#messageInput").val() == "")){
    if(($("#nameInput").val() == "")){
      $("#nameInput").css("border", "2px solid red");
    } else {
      $("#nameInput").css("border", "none");
    }
    if(($("#messageInput").val() == "")){
      $("#messageInput").css("border", "2px solid red");
    } else {
      $("#messageInput").css("border", "none");
    }
    if(($("#emailInput").val() == "")){
      $("#emailInput").css("border", "2px solid red");
    }else {
      $("#emailInput").css("border", "none");
    }
    antwoord = " U bent wat vergeten in te vullen!";
  } else if (!(emailValidation(email))) {
      antwoord = "Uw emailadres klopt niet!"
      $("#emailInput").css("border", "2px solid red");
  } else {
      $("#submit").removeClass("submitError");
      $("#emailInput").css("border", "none");
      $("#submit").addClass("submitSucces");
      $("#submit").html("Succes");
      antwoord = "Uw bericht is verstuurd!"
  }
  $("#response").html(antwoord);
});

$("#submit").mouseenter(function(){
  $("#submit").removeClass("submitSucces");
  $("#submit").html("Verzend");
});




$("#opdracht1Btn").click(function(){
  $(current).removeClass("active").addClass("nonActive");
  $(current + "Btn").removeClass("activeBtn");
  $("#opdracht1Btn").addClass("activeBtn")
  window.setTimeout(function() {
    $("#opdracht1").removeClass("nonActive").addClass("active");
  }, 1000);
  current = "#opdracht1";
});

$("#opdracht2Btn").click(function(){
  $(current).removeClass("active").addClass("nonActive");
  $(current + "Btn").removeClass("activeBtn");
  $("#opdracht2Btn").addClass("activeBtn");
  window.setTimeout(function() {
    $("#opdracht2").removeClass("nonActive").addClass("active");
  }, 1000);
  current = "#opdracht2";
});

$("#opdracht3Btn").click(function(){
  $(current).removeClass("active").addClass("nonActive");
  $(current + "Btn").removeClass("activeBtn");
  $("#opdracht3Btn").addClass("activeBtn");
  window.setTimeout(function() {
    $("#opdracht3").removeClass("nonActive").addClass("active");
  }, 1000);
  current = "#opdracht3";
});

$("#homeBtn").click(function(){
  $(current).removeClass("active").addClass("nonActive");
  $(current + "Btn").removeClass("activeBtn");
  $("#homeBtn").addClass("activeBtn");
  window.setTimeout(function() {
    $("#home").removeClass("nonActive").addClass("active");
  }, 1000);
  current = "#home";
});

$("#contactBtn").click(function(){
  $(current).removeClass("active").addClass("nonActive");
  $(current + "Btn").removeClass("activeBtn");
  $("#contactBtn").addClass("activeBtn");
  window.setTimeout(function() {
    $("#contact").removeClass("nonActive").addClass("active");
  }, 1000);
  current = "#contact";
});
